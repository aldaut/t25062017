Приложение согласно требованиям
Настройки подключение к БД Postgres, в файле - hibernate.cfg.xml

Создание базы:
CREATE DATABASE pets WITH ENCODING 'UTF8' OWNER postgres;

CREATE TABLE app_owner (
	id serial primary key,
	login character varying(255) unique,
	name character varying(255),
	password character varying(255),
	aut_cnt int,
	last_miss_aut timestamp without time zone
);
ALTER TABLE app_owner
  OWNER TO postgres;


CREATE TABLE app_pet_type (
	id serial primary key,
	name character varying(255)
);
ALTER TABLE app_pet_type
  OWNER TO postgres;


CREATE TABLE app_pet (
	id serial primary key,
	name character varying(255) unique,
	born_date date,
	pet_sex int,
	owner bigint references app_owner(id),
	type bigint references app_pet_type(id)
);
ALTER TABLE app_pet
  OWNER TO postgres;


insert into app_pet_type(name) values ('dog');
insert into app_pet_type(name) values ('cat');
insert into app_pet_type(name) values ('mouse');