/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.servlet.http.HttpServletRequest;
import model.pojo.AppOwner;
import model.pojo.RequestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import services.AppOwnerService;
import util.ErrorList;

/**
 *
 * @author almas
 */
@Controller
public class AppOwnerController {

    @Autowired
    private AppOwnerService appOwnerService;

    @RequestMapping("/petslist")
    public String petList() {
        return "petslist";
    }

    @RequestMapping(value = "/signIn", method = RequestMethod.POST)
    public @ResponseBody
    RequestStatus userSignin(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        String login = request.getParameter("login");
        String psswrd = request.getParameter("psswrd");
        try {
            appOwnerService.getAutOwner(login, psswrd);
        } catch (Exception e) {
            return new RequestStatus("ERROR",ErrorList.USER_NOT_AUTARIZED.toString() + e.getMessage());
        }
        return new RequestStatus();
    }

    @RequestMapping(value = "/addOwner", method = RequestMethod.POST)
    public @ResponseBody
    RequestStatus addOwner(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        String ownerName = request.getParameter("ownerName");
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        try {
            AppOwner owner = new AppOwner();
            owner.setLogin(login);
            owner.setName(ownerName);
            owner.setPassword(password);
            appOwnerService.addOwner(owner);
        } catch (Exception e) {
            return new RequestStatus("ERROR",ErrorList.DATABASE.toString() + e.getMessage());
        }
        return new RequestStatus();
    }
}
