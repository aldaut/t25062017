/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.pojo.AppPetType;
import model.pojo.RequestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import services.AppPetTypeService;
import util.ErrorList;

/**
 *
 * @author almas
 */
@Controller
public class AppPetTypeController {

    @Autowired
    private AppPetTypeService appPetTypeService;

    @RequestMapping(value = "/getPetTypes", method = RequestMethod.GET)
    public @ResponseBody
    List getPetTypes(HttpServletRequest request) throws Exception {
        List lst = new ArrayList<AppPetType>();
        try {
            lst = appPetTypeService.getPetTypes();
        } catch (Exception e) {
            RequestStatus reqState = new RequestStatus();
            reqState.setRequestStatus("ERROR");
            reqState.setRequestMessage(ErrorList.DATABASE.toString() + e.getMessage());
            List<RequestStatus> lstRs = new ArrayList<RequestStatus>();
            lstRs.add(reqState);
            return lstRs;
        }
        return lst;
    }
}
