/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.dao.AppOwnerDao;
import model.dao.AppPetTypeDao;
import model.pojo.AppPet;
import model.pojo.RequestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import services.AppPetService;
import util.ErrorList;

/**
 *
 * @author almas
 */
@Controller
public class AppPetController {

    @Autowired
    private AppPetService appPetService;

    @Autowired
    private AppPetTypeDao appPetTypeDao;

    @Autowired
    private AppOwnerDao appOwnerDao;

    @RequestMapping(value = "/getpets", method = RequestMethod.GET)
    public @ResponseBody
    List getPetList(HttpServletRequest request) throws Exception {
        String login = request.getParameter("login");
        List lst = appPetService.getPetsList(login);////
        return lst;
    }

    @RequestMapping(value = "/getpetbyid", method = RequestMethod.GET)
    public @ResponseBody
    List getPet(HttpServletRequest request) throws Exception {
        String sPetId = request.getParameter("petId");
        Integer petId = Integer.parseInt(sPetId);
        List<AppPet> lst;
        try {
            lst = appPetService.getPet(petId);
        } catch (Exception e) {
            RequestStatus reqState = new RequestStatus("ERROR", e.getMessage());
            List<RequestStatus> lstRs = new ArrayList<>();
            lstRs.add(reqState);
            return lstRs;
        }
        return lst;
    }

    @RequestMapping(value = "/petJob", method = RequestMethod.GET)
    public @ResponseBody
    RequestStatus petJob(HttpServletRequest request) throws Exception {
        AppPet pet = new AppPet();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String req = request.getParameter("requesttype");
        try {
            String petName = request.getParameter("petName");
            pet.setName(petName);
            if (!req.equals("delete")) {
                String bornDate = request.getParameter("bornDate");
                String petSex = request.getParameter("petSex");
                String ownerLogin = request.getParameter("ownerLogin");
                String typeName = request.getParameter("typeName");
                pet.setPetSex(Integer.parseInt(petSex));
                pet.setBornDate(format.parse(bornDate));
                pet.setAppOwner(appOwnerDao.getOwnerByLogin(ownerLogin).get(0));
                pet.setAppPetType(appPetTypeDao.getTypeByName(typeName).get(0));
            }
        } catch (Exception e) {
            return new RequestStatus("ERROR", ErrorList.PET_DATA_ERROR.toString() + e.getMessage());
        }

        try {
            switch (req) {
                case "add":
                    appPetService.addPet(pet);
                    break;
                case "edit":
                    appPetService.editPet(pet);
                    break;
                case "delete":
                    appPetService.deletePet(pet);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            return new RequestStatus("ERROR", ErrorList.DATABASE.toString() + e.getMessage());
        }
        return new RequestStatus();
    }
}
