/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author almas
 */
public class Parameters {
    private static int missedAutCnts = 10; //количество неудачных входов в логин
    private static long missedAutTimeMS = 3600000; //время после которого кол-во неудачных входов в логин сбрасывается

    public static int getMissedAutCnts() {
        return missedAutCnts;
    }

    public static long getMissedAutTimeMS() {
        return missedAutTimeMS;
    }

    
}
