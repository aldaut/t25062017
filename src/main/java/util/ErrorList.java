/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author almas
 */
public enum ErrorList {
    DATABASE(0, "Возникла ошибка при обращнии к базе данных : "),
    USER_DUPLICATE_LOGIN(1, "Пользователь с таким логином уже заведен "),
    USER_AUTCNT_EXCEED(2, "Превышено количество допустимых попыток авторизации " + Parameters.getMissedAutCnts()),
    USER_NOT_AUTARIZED(3, "Не корректный пароль или логин "),
    PET_DUPLICATE_LOGIN(4, "Животное с такой кличкой уже заведено "),
    PET_DATA_ERROR(5, "Ошибка данных "),
    PET_BYID_NOT_FOUND(6, "По заданному идентификатору животное не найдено "),
    USER_DATA_NOT_FULL(7, "Данные пользвателя не полны "),
    USER_NOT_AUTORIZED(8, "Пользователь не авторизован ")
    ;
            
    private final int code;
    private final String description;

    private ErrorList(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Ошибка №" + code + ": " + description;
    }
}
