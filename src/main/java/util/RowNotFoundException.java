/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author almas
 */
public class RowNotFoundException extends Exception {

    public RowNotFoundException() {
    }

    // Constructor that accepts a message
    public RowNotFoundException(String message) {
        super(message);
    }
}
