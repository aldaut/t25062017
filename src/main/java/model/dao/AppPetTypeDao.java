/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.pojo.AppPetType;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import util.HibernateUtil;

/**
 *
 * @author almas
 */
@Repository
public class AppPetTypeDao {

    public List<AppPetType> getPetTypes() throws Exception {
        List<AppPetType> lst = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "from AppPetType";
            Query query = session.createQuery(hql);
            lst = query.list();
            session.close();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return lst;
    }

    public List<AppPetType> getTypeByName(String typeName) throws Exception {
        List<AppPetType> lst = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "from AppPetType where Name = :typeName";
            Query query = session.createQuery(hql);
            query.setParameter("typeName", typeName);
            lst = query.list();
            session.close();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return lst;
    }
}
