/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.pojo.AppOwner;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import util.ErrorList;
import util.HibernateUtil;

/**
 *
 * @author almas
 */
@Repository
public class AppOwnerDao {

    public List<AppOwner> getOwnerList() throws Exception {
        List<AppOwner> lst = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "from AppOwner";
            Query query = session.createQuery(hql);
            lst = query.list();
            session.close();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return lst;
    }

    //список пользователей с паролями
    public List<AppOwner> getOwnerByLogin(String login) throws Exception {
        List<AppOwner> lst = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "from AppOwner where login= :login";
            Query query = session.createQuery(hql);
            query.setParameter("login", login);
            lst = query.list();
            session.close();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return lst;
    }

    //количество авторизации
    public void updateOwner(AppOwner ownr) throws Exception {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(ownr);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    //добавим пользователя
    public void addOwner(AppOwner owner) throws Exception {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List lst = getOwnerByLogin(owner.getLogin());
            if (lst.isEmpty()) {
                session.beginTransaction();
                session.save(owner);
                session.getTransaction().commit();
            } else {
                session.close();
                throw new Exception(ErrorList.USER_DUPLICATE_LOGIN.toString());
            }
            session.close();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
