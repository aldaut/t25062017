/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.pojo.AppPet;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import util.ErrorList;
import util.HibernateUtil;
import util.RowNotFoundException;

/**
 *
 * @author almas
 */
@Repository
public class AppPetDao {

    public List<AppPet> getPetsList(String login) throws Exception {
        List lst = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "from AppPet as p join p.appOwner as o with o.login= :login";
            Query query = session.createQuery(hql);
            query.setParameter("login", login);
            lst = query.list();
            session.close();
            // lst.get(0).getAOwner().setPassword("XXX");
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return lst;
    }

    public List<AppPet> getPetbyId(Integer petId) throws Exception {
        List<AppPet> lst = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "from AppPet as p where p.id = :petId";
            Query query = session.createQuery(hql);
            query.setParameter("petId", petId);
            lst = query.list();
            session.close();
            if (lst.isEmpty()) {
                throw new Exception(ErrorList.PET_BYID_NOT_FOUND.toString());
            }
            lst.get(0).getAppOwner().setPassword("XXX");
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return lst;
    }

    public AppPet getPetbyName(String petName) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        AppPet appPet;
        try {
            String hql = "from AppPet where name= :petName";
            Query query = session.createQuery(hql);
            query.setParameter("petName", petName);
            appPet = (AppPet) query.uniqueResult();
            session.close();
            if (appPet == null) {
                throw new RowNotFoundException(ErrorList.PET_BYID_NOT_FOUND.toString());
            }
            appPet.getAppOwner().setPassword("XXX");
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return appPet;
    }

    /*    public List<AppPet> getPetbyName(String petName) throws Exception {
        List<AppPet> lst = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "from AppPet where name= :petName";
            Query query = session.createQuery(hql);
            query.setParameter("petName", petName);
            lst = query.list();
            session.close();
            if (lst.isEmpty()) {
                throw new Exception(ErrorList.PET_BYID_NOT_FOUND.toString());
            }
            lst.get(0).getAppOwner().setPassword("XXX");
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return lst;
    }
     */
    public void addPet(AppPet pet) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.save(pet);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void updatePet(AppPet pet) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.update(pet);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void deletePet(AppPet pet) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.clear();
            session.beginTransaction();
            session.delete(pet);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
