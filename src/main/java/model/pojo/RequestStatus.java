/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.pojo;

/**
 *
 * @author almas
 */
public class RequestStatus {

    private String requestStatus;
    private String requestMessage;

    public RequestStatus() {
        this.requestStatus = "OK";
    }

    public RequestStatus(String requestStatus, String requestMessage) {
        this.requestStatus = requestStatus;
        this.requestMessage = requestMessage;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
    }

}
