package model.pojo;
// Generated 23.07.2017 19:46:25 by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

/**
 * AppPetType generated by hbm2java
 */
public class AppPetType implements java.io.Serializable {

    private int id;
    private String name;
    private Set appPets = new HashSet(0);

    public AppPetType() {
    }

    public AppPetType(int id) {
        this.id = id;
    }

    public AppPetType(int id, String name, Set appPets) {
        this.id = id;
        this.name = name;
        this.appPets = appPets;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set getAppPets() {
        return this.appPets;
    }

    public void setAppPets(Set appPets) {
        this.appPets = appPets;
    }

}
