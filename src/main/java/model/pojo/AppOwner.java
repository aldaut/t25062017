package model.pojo;
// Generated 23.07.2017 19:46:25 by Hibernate Tools 4.3.1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * AppOwner generated by hbm2java
 */
public class AppOwner implements java.io.Serializable {

    private int id;
    private String login;
    private String name;
    private String password;
    private Integer autCnt;
    private Date lastMissAut;
    private Set appPets = new HashSet(0);

    public AppOwner() {
    }

    public AppOwner(int id) {
        this.id = id;
    }

    public AppOwner(int id, String login, String name, String password, Integer autCnt, Date lastMissAut, Set appPets) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.password = password;
        this.autCnt = autCnt;
        this.lastMissAut = lastMissAut;
        this.appPets = appPets;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAutCnt() {
        return this.autCnt;
    }

    public void setAutCnt(Integer autCnt) {
        this.autCnt = autCnt;
    }

    public Date getLastMissAut() {
        return this.lastMissAut;
    }

    public void setLastMissAut(Date lastMissAut) {
        this.lastMissAut = lastMissAut;
    }

    public Set getAppPets() {
        return this.appPets;
    }

    public void setAppPets(Set appPets) {
        this.appPets = appPets;
    }

}
