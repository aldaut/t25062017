/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.dao.AppOwnerDao;
import model.pojo.AppOwner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.ErrorList;
import util.Parameters;

/**
 *
 * @author almas
 */
@Service
public class AppOwnerServiceImpl implements AppOwnerService {

    @Autowired
    private AppOwnerDao appOwnerDao;

    @Override
    public void addOwner(AppOwner owner) throws Exception {
        if (!owner.getLogin().isEmpty() || !owner.getPassword().isEmpty()||!owner.getName().isEmpty() ) {
            appOwnerDao.addOwner(owner);   
        } else 
            throw new Exception(ErrorList.USER_DATA_NOT_FULL.toString()); 
    }

    @Override
    public List getOwnerList() throws Exception {
        List<AppOwner> lst = appOwnerDao.getOwnerList();
        return lst;
    }

    @Override
    public void getAutOwner(String login, String psswrd) throws Exception {

        List<AppOwner> lst = new ArrayList<>();
        try {
            lst = getOwnerList();
        } catch (Exception e) {
            throw new Exception(ErrorList.DATABASE.toString() + e.getMessage());
        }
        boolean isAutorized = false;
        for (AppOwner owner : lst) {
            if (owner.getLogin().equals(login)) {
                //дата авторизации =если количество авторизации=0 то сбросим дату, если 1 то запишем
                if (owner.getPassword().equals(psswrd)) {
                    owner.setAutCnt(0);
                    owner.setLastMissAut(null);
                    isAutorized = true;
                } else {
                    owner.setAutCnt(owner.getAutCnt() + 1);
                    owner.setLastMissAut(new Date(System.currentTimeMillis()));
                    if (System.currentTimeMillis() - owner.getLastMissAut().getTime()
                            > Parameters.getMissedAutTimeMS()) {
                        owner.setAutCnt(0);
                        owner.setLastMissAut(null);
                    }
                    if (owner.getAutCnt() > Parameters.getMissedAutCnts()) {
                        throw new Exception(ErrorList.USER_AUTCNT_EXCEED.toString());
                    }
                }
                appOwnerDao.updateOwner(owner);
            }
        }
        if (!isAutorized) {
            throw new Exception(ErrorList.USER_NOT_AUTORIZED.toString());
        }
    }
}
