/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.List;

/**
 *
 * @author almas
 */
public interface AppPetTypeService {

    public List getPetTypes() throws Exception;

    public List getTypeByName(String typeName) throws Exception;
}
