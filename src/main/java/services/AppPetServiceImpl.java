/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.List;
import model.dao.AppPetDao;
import model.pojo.AppPet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.ErrorList;
import util.RowNotFoundException;

/**
 *
 * @author almas
 */
@Service
public class AppPetServiceImpl implements AppPetService {

    @Autowired
    private AppPetDao appPetDao;

    @Override
    public List getPetsList(String login) throws Exception {
        if (login.isEmpty()) {
            throw new Exception(ErrorList.PET_DATA_ERROR.toString());
        }
        List lst = appPetDao.getPetsList(login);
        return lst;
    }

    @Override
    public List getPet(Integer petId) throws Exception {
        List<AppPet> lst = appPetDao.getPetbyId(petId);
        return lst;
    }

    @Override
    public void addPet(AppPet pet) throws Exception {
        try {
            AppPet appPet = appPetDao.getPetbyName(pet.getName());
        } catch (RowNotFoundException e) {
            appPetDao.addPet(pet);
        }
    }

    @Override
    public void editPet(AppPet pet) throws Exception {
        AppPet appPet = appPetDao.getPetbyName(pet.getName());
        appPet.setAppPetType(pet.getAppPetType());
        appPet.setBornDate(pet.getBornDate());
        appPet.setName(pet.getName());
        appPet.setPetSex(pet.getPetSex());
        appPetDao.updatePet(appPet);
    }

    @Override
    public void deletePet(AppPet pet) throws Exception {
        AppPet appPet = appPetDao.getPetbyName(pet.getName());
        appPetDao.deletePet(appPet);
    }
}
