/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.List;
import model.pojo.AppPet;

/**
 *
 * @author almas
 */
public interface AppPetService {

    public List getPetsList(String login) throws Exception;

    public List getPet(Integer petId) throws Exception;

    public void addPet(AppPet pet) throws Exception;

    public void editPet(AppPet pet) throws Exception;

    public void deletePet(AppPet pet) throws Exception;
}
