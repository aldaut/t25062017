/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.List;
import model.dao.AppPetTypeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author almas
 */
@Service
public class AppPetTypeServiceImpl implements AppPetTypeService {

    @Autowired
    private AppPetTypeDao appPetTypeDao;

    @Override
    public List getPetTypes() throws Exception {
        List lst = appPetTypeDao.getPetTypes();
        return lst;
    }

    @Override
    public List getTypeByName(String typeName) throws Exception {
        List lst = appPetTypeDao.getTypeByName(typeName);
        return lst;
    }

}
