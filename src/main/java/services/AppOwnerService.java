/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.List;
import model.pojo.AppOwner;

/**
 *
 * @author almas
 */
public interface AppOwnerService {

    public void addOwner(AppOwner owner) throws Exception;

    public List getOwnerList() throws Exception;

    public void getAutOwner(String login, String psswrd) throws Exception;

}
