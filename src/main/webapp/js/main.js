/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.onload = function () {
    $("#catalogs").hide()
    $("#catalogsById").hide()
    $("#openModal").hide()
};

function hideModal() {
    $("#openModal").hide()
}

function ajaxRequetPost(data, url) {
    var json = $.ajax({
        data: data,
        url: url,
        dataType: "json",
        type: "POST",
        async: false
    });
    return json.responseText;
}
function getOwnerPets(login) {
    var html;
    $.ajax({
        data: {
            login: login
        },
        url: "getpets.html", //дай животных
        dataType: "json",
        type: "GET",
        success: function (json) {
            html = '<table id ="petTable"><thead><tr><th>ID</th><th>Кличка</th><th>Пол</th><th>Вид животного</th><th>Дата рождения</th><th>Изменить</th></tr></thead><tbody>'

            for (var j = 0; j < json.length; j++) {
                for (var i = 0; i < json[j].length - 1; i++) {
                    var date = new Date(json[j][i].bornDate);
                    var day = date.getDate();
                    var month = date.getMonth();
                    var year = date.getFullYear();
                    var dateString = day + '/' + month + '/' + year;
                    html += '<tr><td>' + json[j][i].id + '</td>';
                    html += '<td>' + json[j][i].name + '</td>';
                    html += '<td>' + json[j][i].petSex + '</td>';
                    html += '<td>' + json[j][i].appPetType.name + '</td>';
                    html += '<td>' + dateString + '</td>';
                    html += '<td><a href="#openModal"><button class = "submit" id="butedit" onclick="openModal(\&#39edit\&#39,' + j + ')">Изменить</button></td>';
                    html += '<td style="display:none">' + json[j][i].appOwner.login + '</td></tr>';
                }
            }
            html += '</tbody></table>'
            $("#catalogsOutput").html(html)
            $("#catalogs").show("fast");
            $("#catalogsById").show("fast");
        }
    })
}
// получение типов животных
function openModal(elem, idx) {
    $("#openModal").show()
    var html;
    $.ajax({
        data: {},
        url: "getPetTypes.html",
        dataType: "json",
        type: "GET",
        success: function (json) {
            html = '<select id = "selPetType" size="1">'
            for (var j = 0; j < json.length; j++) {
                html += '<option value="' + json[j].name + '">' + json[j].name + '</option>';
            }
            html += '</select>';
            $("#modalselect").html(html)
            if (elem !== "add") {
                var table = document.getElementById('petTable'),
                        rows = table.getElementsByTagName('tr'),
                        i, j, cells, customerId;

                for (i = 0, j = rows.length; i < j; ++i) {
                    cells = rows[i].getElementsByTagName('td');
                    if (!cells.length) {
                        continue;
                    }
                    if (i === idx + 1) {
                        $("#petName").val(cells[1].innerHTML)
                        $("#petSex").val(cells[2].innerHTML)
                        $("#selPetType").val(cells[3].innerHTML);
                        var parts = cells[4].innerHTML.split('/');
                        if (parts[1] < 10) {
                            parts[1] = "0" + parts[1]
                        }
                        if (parts[0] < 10) {
                            parts[0] = "0" + parts[0]
                        }
                        $("#bornDate").val(parts[2] + "-" + parts[1] + "-" + parts[0])
                    }
                }
                $("#petId").show()
                $("#modaltext").text("Изменить животное")
                $("#submitPetAdd").hide()
                $("#submitPetEdit").show()
                $("#submitPetDell").show()
                $("#petName").prop("readonly", true);
            } else {
                $("#petId").val("")
                $("#petName").val("")
                $("#bornDate").val("")
                $("#petId").hide()
                $("#modaltext").text("Добавить животное")
                $("#submitPetAdd").show()
                $("#submitPetEdit").hide()
                $("#submitPetDell").hide()
                $("#petName").prop("readonly", false);
            }
        }
    })
}
//авторизация
function butOwnerAut() {
    var errorText = "";
    $("#catalogs").hide()
    $("#catalogsById").hide()
    if ($("#alogin").val() === '') {
        errorText += "Пожалуйста заполните Логин\n";
    }
    if ($("#apsswrd").val() === '') {
        errorText += "Пожалуйста заполните Пароль\n";
    }

    if (errorText !== '') {
        alert(errorText);
    } else {
        //авторизация
        var data = {};
        data["login"] = $("#alogin").val();
        data["psswrd"] = $("#apsswrd").val();

        var json = {}
        json = $.parseJSON(ajaxRequetPost(data, "signIn.html"));//авторизация
        if (json.requestStatus === 'ERROR') {
            alert(json.requestMessage)
        } else if (json.requestStatus === 'OK') {
            $("#ownerLoginM").text($("#alogin").val())
            $("#featured_area").hide()
            $("#introduction").text("Добро пожаловать " + $("#alogin").val())
            getOwnerPets($("#alogin").val());
        }
    }
}
//регистрация
function butOwnerAdd() {
    var errorText = "";
    $("#catalogs").hide()
    $("#catalogsById").hide()
    if ($("#name").val() === '') {
        errorText += "Пожалуйста заполните ваше Имя\n";
    }
    if ($("#login").val() === '') {
        errorText += "Пожалуйста заполните Логин\n";
    }
    if ($("#psswrd").val() === '') {
        errorText += "Пожалуйста заполните Пароль\n";
    }

    if (errorText !== '') {
        alert(errorText);
    } else {
        var json = {}
        var data = {};
        data["ownerName"] = $("#name").val();
        data["login"] = $("#login").val();
        data["password"] = $("#psswrd").val();
        json = $.parseJSON(ajaxRequetPost(data, "addOwner.html"));
        if (json.requestStatus === 'ERROR') {
            alert(json.requestMessage);
        } else if (json.requestStatus === 'OK') {
            $("#ownerLoginM").text($("#login").val())
            $("#featured_area").hide()
            $("#introduction").text("Добро пожаловать " + $("#login").val())
            getOwnerPets($("#login").val());
        }
    }
}

function getPetById() {
    var errorText = "";
    if ($("#rowId").val() === '') {
        errorText += "Пожалуйста заполните id животного\n";
    } else if (!$("#rowId").val().match(/^\d+$/)) {
        errorText += "Можно вводить только цифры\n";
    }
    if (errorText !== '') {
        alert(errorText)
    } else {
        var json = {}
        $.ajax({
            data: {
                petId: $("#rowId").val()
            },
            url: "getpetbyid.html", //дай животных
            dataType: "json",
            type: "GET",
            success: function (json) {
                if (json[0].requestStatus === 'ERROR') {
                    alert(json[0].requestMessage);
                } else {
                    var html = '<table><thead><tr><th>ID</th><th>Кличка</th><th>Пол</th><th>Вид животного</th><th>Дата рождения</th></tr></thead><tbody>'
                    var date = new Date(json[0].bornDate);
                    var day = date.getDate();
                    var month = date.getMonth();
                    var year = date.getFullYear();
                    var dateString = day + '/' + month + '/' + year;
                    html += '<tr><td>' + json[0].id + '</td>';
                    html += '<td>' + json[0].name + '</td>';
                    html += '<td>' + json[0].petSex + '</td>';
                    html += '<td>' + json[0].appPetType.name + '</td>';
                    html += '<td>' + dateString + '</td></tr>';
                    html += '</tbody></table>'
                    $("#rowOutput").html(html)
                }
            }
        })
    }
}

function butPetJob(action) {
    if ($("#ownerLoginM").text() === "") {
        $("#openModal").hide()
        return;
    }

    var errorText = "", data = {};
    if (action !== 'delete') {
        $("#catalogs").hide()
        $("#catalogsById").hide()
        if ($("#petName").val() === '') {
            errorText += "Пожалуйста заполните кличку животного\n";
        } else if ($("#bornDate").val() === '') {
            errorText += "Пожалуйста заполните дату рождения животного\n";
        } else if ($("#petSex option:selected").val() === '') {
            errorText += "Пожалуйста выберете пол животного\n";
        } else if ($("#selPetType option:selected").text() === '') {
            errorText += "Пожалуйста выберете вид животного\n";
        }
        data["bornDate"] = $("#bornDate").val();
        data["petSex"] = $("#petSex option:selected").val();
        data["ownerLogin"] = $("#ownerLoginM").text();
        data["typeName"] = $("#selPetType option:selected").text();
    }
    data["petName"] = $("#petName").val();
    data["requesttype"] = action
    if (errorText !== '') {
        alert(errorText)
    } else {
        var json = {}
        $.ajax({
            data: data,
            url: "petJob.html", //дай животных
            dataType: "json",
            type: "GET",
            success: function (json) {
                if (json.requestStatus === 'ERROR') {
                    alert(json.requestMessage);
                } else {
                    $("#openModal").hide()
                    getOwnerPets($("#ownerLoginM").text());
                }

            }
        })
    }
    getOwnerPets($("#ownerLoginM").text());
}
    