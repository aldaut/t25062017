<%-- 
    Document   : petslist
    Created on : 18.07.2017, 14:07:51
    Author     : almas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src= "<c:url value="/js/main.js "/>"></script>
        <script type="text/javascript" src="<c:url value="/js/jquery-1.7.0.min.js " />"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="<c:url value="css/styles.css" />"/>    
        <title>JSP Page</title>
    </head>
    <body>
        <div class="wrap">
            <div id="header">
                <h1 id="introduction"></h1> 
            </div>		
        </div>

        <div id="featured_area">
            <div class="wrap gap">
                <div class="featured_form">
                    <fieldset id="signupform">
                        <div>
                            <input type="text" id="login" maxlength="35" placeholder ="Логин"/>
                            <input type="text" id="name" maxlength="35" placeholder ="Имя"/>
                            <input type="password" id="psswrd" maxlength="35" placeholder ="Пароль" autocomplete="off" />
                            <button type="submit" class = "submit" id="subreg" onclick="butOwnerAdd()">Зарегистрироваться</button>    
                        </div>
                    </fieldset>
                </div>
                <div class="featured_text">                    
                    <h2>Авторизоваться</h2>
                    <p>Гарантия вашего безбедного будущего. Шаг к финансовой свободе. 
                        Или просто способ стать красивой.
                    </p>
                    <fieldset id="autorizform">
                        <div>
                            <input type="text" id="alogin" maxlength="35" placeholder ="Логин"/>
                            <input type="password" id="apsswrd" maxlength="35" placeholder ="Пароль" autocomplete="off" />
                            <button type="submit" class = "submit" id="subaut" onclick="butOwnerAut()">Авторизоваться</button>
                        </div>
                    </fieldset>    
                </div>
            </div>
        </div>
        <div class="wrap gap" id = "catalogsById">
            <div id="rowOutput"></div>
            <div style="width: 20%;">
                <input type="text" id="rowId" maxlength="35" placeholder ="id животного"/>
                <button type="submit" class = "submit" id="subaut" onclick="getPetById()">Найти</button>
            </div>
        </div>

    </div>
    <div class="wrap gap" id = "catalogs">
        <div id="catalogsOutput"></div>
        <div class = "featured_buttons">
            <a href="#openModal"><button class = "submit" id="butadd"  onclick="openModal('add')">Добавить</button></a>
        </div>
    </div>
    <div id="openModal" class="modalDialog">
        <div>
            <a href="#close" title="Закрыть" class="close" id="closeModal" onclick="hideModal()">X</a>
            <fieldset id="signupform">
                <h2 id = "modaltext"></h2>
                <div>
                    <input type="text" id="petName" maxlength="35" placeholder ="Кличка животного" readonly/>
                    <input type="date" id="bornDate" placeholder ="Дата рождения" />
                    <select id ='petSex' size="1">
                        <option disabled>Выберите пол животного</option>
                        <option value="0">Мужской</option>
                        <option value="1">Женский</option>
                    </select>
                    <div id="modalselect">
                    </div>
                    <button type="submit" class = "submit" id="submitPetAdd" onclick="butPetJob('add')">Добавить</button>
                    <button type="submit" class = "submit" id="submitPetEdit" onclick="butPetJob('edit')">Изменить</button>
                    <button type="submit" class = "submit" id="submitPetDell" onclick="butPetJob('delete')">Удалить</button>
                </div>
            </fieldset>
            <p id = "autError"></p>
        </div>
    </div>
    <input type="hidden" id="ownerLoginM" />
    <div class="prefooter"></div>
    <footer class="footer">   
        <p>Almaty 2016</p>
    </footer>     
</div>
</body>
</html>
